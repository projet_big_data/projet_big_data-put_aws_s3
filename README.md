# projet_big_data-put_aws_s3

Projet permettant d'exporter des données stocké en local sur AWS S3

## Utilisation

Pour le bon dérouler de ce script il est nécessaire de pouvoir utiliser les commandes AWS CLI.

### Parametration

Les variables à modifié sont dans le fichier **env.config**

La variable *aws_s3* correspond au nom du bucket ou déposer les fichiers
La variable *fs_root* correspond au dossier ou se trouvent les données.

### Lancement 

Utilisez la commande
```
./put_file.sh
```
